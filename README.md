## installation
```sh
git clone https://gitlab.com/amiremohamadi/jiz.git
cd jiz

virtualenv -p python3 .venv
source .venv/bin/activate

pip install -r requirements.txt

python __main__.py
```

## configure
type these commands (inorder?)
```sh
gpg-path path_to_gpg # default is current dir (eg. gpg-path .)

gpg-create your_gpg_name your_gpg_password

# export and give your public key to a friend
gpg-export your_gpg_name file_name

# login to lms
login lms_username lms_password

# a number appear in left side (eg. [123]) that's your call id (give it to friend)
# ask your friend to give you, his/her id and public gpg key
# add friend to your phone book
add friend_id path_to_his_gpg_file

# call!
connect friend_id

# done!

```