import requests
import bs4


class API:
    def __init__(self, username, password):
        self._base_url = 'http://oldlms.birjand.ac.ir'
        self._cookie = None
        self._sesskey = None
        self._userid = None
        self.username = username
        self.password = password


    def _get_sesskey(self):
        '''get sesskey'''
        response = requests.get(self._base_url, cookies=self._cookie)
        soup = bs4.BeautifulSoup(response.content, 'html.parser')

        # quit button in menubar contains a link with sesskey
        # the idea is to find it and extract sesskey
        exit_button = soup.select_one('.menu-action:nth-child(8)')

        # we found it? so get the link
        try:
            link = exit_button['href']
            # TODO: find a better way to extract sesskey from link
            self._sesskey = link.split('=')[1]
        except (KeyError, IndexError, TypeError):
            return False
        return True


    def _get_userid(self):
        '''get userid'''
        response = requests.get(self._base_url, cookies=self._cookie)
        soup = bs4.BeautifulSoup(response.content, 'html.parser')

        # message button contains userid
        message_button = soup.find('div', id='nav-message-popover-container')

        # user-id attribute contains user id
        try:
            self._userid = message_button.get('data-userid', None)
        except:
            return False
        return True


    def login(self):
        '''
        login to lms.birjand.ac.ir
        @return: True/False
        '''
        login_url = self._base_url + '/login/index.php'
        payload = {
                'anchor': '',
                'username': self.username,
                'password': self.password
            }

        session = requests.Session()
        session.post(login_url, data=payload)
        self._cookie = requests.utils.dict_from_cookiejar(session.cookies)
        self._get_sesskey()
        self._get_userid()

        return self._sesskey and self._userid and self._cookie


    def send(self, text, other_uid):
        '''
        send message to a userid
        @parameters: text:string, other_uid:string
        @return: messageid:int. in case of failing returns -1
        '''
        messages_url = self._base_url + f'/lib/ajax/service.php?sesskey={self._sesskey}'
        data = [{ 
            "args": {
                "messages": [{
                    "text": text, 
                    "touserid": f'{other_uid}'
                    }
                ]}, 
            "index": 0,
            "methodname": "core_message_send_instant_messages"
            } 
        ] 

        try:
            response = requests.post(messages_url, json=data, cookies=self._cookie)
            response = response.json()[0] # lms api is very tokhmi

            # TODO: refactor this shit
            msgid = response.get('data', [{}])[0]['msgid']
        except:
            return -1
        return msgid


    def get_messages(self, other_uid, timefrom):
        '''
        get messages from lms
        @parameters: other_uid:string, timefrom: int
        @return: messages:list 
        '''
        messages_url = self._base_url + f'/lib/ajax/service.php?sesskey={self._sesskey}'
        data = [{                                                                      
            "args": { 
                "currentuserid": f'{self._userid}',
                "otheruserid": f'{other_uid}',
                "timefrom": f'{timefrom}',
                "newest": True,
            },
            "methodname": "core_message_data_for_messagearea_messages",
            }
        ]

        response = requests.post(messages_url, json=data, cookies=self._cookie)

        try:
            # sa I said lms is tokhmi!
            response = response.json()[0]

            messages = response['data']['messages']

            # shapeshift list to a list of tuples contains id, sender and text
            for idx, message in enumerate(messages):
                text = bs4.BeautifulSoup(message['text'], 'html.parser').text
                sender = message['useridfrom']
                id = message['id']
                messages[idx] = (id, sender, text)
        except:
            return []
        return messages

