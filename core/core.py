# using lms.birjand.ac.ir as a massenger server
# to be also available in National Information Network
# core file: all executable actions are handled here

import threading
import readline
import time
import json
import os
import gnupg
import core.lms as lms
from core.parser import *


ids = set()                     # store seen messages (message id)
gpg = None                      # store GPG object
api = lms.API('', '')           # lms python api

status = {
    'resmessage': '',

    'command_mode': True,
    'loggedin': False,

    'userid': 'not logged in',  # your id, share this with a friend
    'other_userid': '',         # user you're chatting with
    'last_seen': 0,             # last seen date in unix epoch time
    
    'gpg_path': '',             # gpg files path
    'phone_book': {}            # stores your contacts (userid: fingerprint)
}


def help():
    message = '''
    +------------+----------------------------------+----------------------+
    | command    | usage                            | description          |
    +------------+----------------------------------+----------------------+
    | login      | login user pass                  | login to lms account |
    +------------+----------------------------------+----------------------+
    | connect    | connect userid                   | connect to chat      |
    +------------+----------------------------------+----------------------+
    | disconnect | disconnect                       | leave connect mode   |
    +------------+----------------------------------+----------------------+
    | help       | help                             | show this usage      |
    +------------+----------------------------------+----------------------+
    | gpg-create | gpg-create gpg_username gpg_pass | cerate gpg key       |
    +------------+----------------------------------+----------------------+
    | gpg-export | gpg-export gpg_username path     | export keys          |
    +------------+----------------------------------+----------------------+
    | gpg-path   | gpg-path path                    | change gpg directory |
    +------------+----------------------------------+----------------------+
    '''
    status['resmessage'] = message


def _config_file_path():
    '''return the actual path to configure.json'''
    directory = os.path.dirname(os.path.realpath(__file__))
    config_file = os.path.join(directory, 'configure.json')

    return config_file


def _load_config_data():
    '''load config data from config file'''
    config_dir = _config_file_path() 
    
    try:
        with open(config_dir, 'r') as config:
            data = json.load(config)
    except FileNotFoundError:
        print('could not find config file: no such file or directory')
        exit(1)
    except json.decoder.JSONDecodeError as jerror:
        print(f'json file is not valid: {jerror}')

    return data


def _save_config_data(data):
    '''save config data to config file'''
    config_dir = _config_file_path() 
    
    try:
        with open(config_dir, 'w') as config:
            json.dump(data, config)
    except FileNotFoundError:
        print('could not find config file: no such file or directory')
        exit(1)


def _print_messages(msgs):
    '''print list of messages (help function for get_new_messages)'''
    global gpg
    is_loop_runned = False # return false if no messages printed otherwise true

    for id, sender, message in msgs:
        # avoid printing a message multiple times
        if id in ids:
            continue
        
        # push new message id to ids to prevent printing messages twice
        ids.add(id)
        is_loop_runned = True
        
        # decrypt messages, for now ignore messages that can't be
        # decrypted or just empty messages
        decrypted = ''
        try:
            decrypted = gpg.decrypt(message).data.decode('utf8')
        except:
            pass

        message = decrypted if decrypted else message
        print(f'\n[{sender}]: {message}')

    return is_loop_runned


def load_config():
    '''
    load config file and update status
    config contains last seen which is last time you seen a message,
    path to gpg keys and a phone book with all your contacts fingerprint
    '''
    global status
    global gpg
    data = _load_config_data()

    try:
        status['last_seen'] = data['last_online']
        status['gpg_path'] = data['gpg_directory']
        status['phone_book'] = data['phone_book']
        # import gpg keys to use in chat encryption, it contains all keys
        gpg = gnupg.GPG(gnupghome=status['gpg_path'])
    except KeyError as key:
        print(f'config file opened but key {key} not found')
        exit(1)
    except ValueError:
        print('could not find gpg dir, edit core/configure.json and retry')
        exit(1)


def add_contact(response):
    '''add a contact to phone book'''
    global status
    global gpg
    id = response['id']
    public_key_dir = response['gpg_key'] # file to contact public key 
    data = _load_config_data()

    # load key file to read public key
    try:
        with open(public_key_dir, 'r') as pkey:
            public_key = pkey.read()
        
        # import public key and get fingerprint as an id to be able to retrive
        # key with this fingerprint
        fingerprints = gpg.import_keys(public_key).fingerprints
        # gpg module returns all fingerprints, we need only one
        fingerprint = fingerprints[0]

        data['phone_book'][id] = fingerprint

    except KeyError as key:
        print(f'config file opened but key {key} not found')
        exit(1)

    except IndexError:
        status['resmessage'] = '[err]: fingerprint is empty, check your public key'

    except AttributeError:
        print('gpg key is invalid')

    except FileNotFoundError:
        status['resmessage'] = '[err]: public key file not found'
    
    else:
        _save_config_data(data)
        # update status values
        load_config()
        status['resmessage'] = '[ok]: add contact'


def gpg_set_directory(response):
    '''set gpg directory'''
    global status
    gpg_path = response['directory']
    data = _load_config_data()

    try:
        data['gpg_directory'] = gpg_path 
    except KeyError as key:
        print(f'config file opened but key {key} not found')
        exit(1)

    _save_config_data(data)

    # update status values
    load_config()

    status['resmessage'] = '[ok]: set gpg directory'


def gpg_create_key(response):
    '''create gpg key'''
    global status
    global gpg
    name = response['gpg_name']
    passphrase = response['gpg_passphrase']

    gpg_data = gpg.gen_key_input(name_email=name, passphrase=passphrase)
    gpg.gen_key(gpg_data)

    status['resmessage'] = '[ok]: create gpg'


def gpg_export(response):
    '''export gpg key'''
    global status
    global gpg
    directory = response['directory']
    name = response['gpg_name']

    with open(directory, 'w') as file:
        public_key = gpg.export_keys(name)
        file.write(public_key)

    status['resmessage'] = '[ok]: export key'


def get_new_messages():
    global status
    global api
    buffer = readline.get_line_buffer()
    prompt = f'\n[{status["userid"]}]: {buffer}'

    new_messages = api.get_messages(
            status['other_userid'],
            status['last_seen'],
        )

    any_message_printed = _print_messages(new_messages)

    # resume and keep user buffer
    if any_message_printed:
        print(prompt, end='', flush=True)

    # update last_seen to fetch new messages in every run 
    status['last_seen'] = int(time.time())
    # repeat each 5 seconds
    threading.Timer(5, get_new_messages).start()


def status_reset():
    '''reset status to first state'''
    global status
    status['command_mode'] = True
    status['loggedin'] = False
    status['other_userid'] = ''
    status['userid'] = 'not logged in'


def login(response):
    '''execute login'''
    global status 
    global api
    username, password = response['username'] , response['password']

    api = lms.API(username, password)
    success = api.login()
    
    if success:
        status['loggedin'] = True
        status['userid'] = api._userid
        status['resmessage'] = '[ok]: logged in'
    else:
        status_reset()
        status['resmessage'] = '[err]: could not login'


def send_message(msg):
    global status
    global ids
    other_uid = status['other_userid']

    # ignore empty messages
    if not msg:
        status['resmessage'] = ''
        return

    try:
        gpg_key = status['phone_book'][other_uid]
        # encrypt and send
        msg = gpg.encrypt(
                msg, gpg_key, always_trust=True).data.decode('utf8')
        id = api.send(msg, other_uid)
        # keep track of message id to prevent printing multiple times
        ids.add(id)

    except KeyError:
        status['resmessage'] = '[err]: could not find contact gpg in phone book'

    except:
        status['resmessage'] = '[err]: could not send message :('
    
    else:
        status['resmessage'] = '[ok]: sent'


def connect(response):
    '''connect to a user'''
    global status

    if not status['loggedin']:
        status['resmessage'] = '[err]: you must login before connecting'
    else:
        status['other_userid'] = response['addr']
        status['resmessage'] = f'[ok]: connected to {status["other_userid"]}'
        status['command_mode'] = False
        get_new_messages()


def disconnect():
    '''disconnect from chat and return to command mode'''
    global status
    status['resmessage'] = '[ok]: disconnected. now you can type a command'
    status['command_mode'] = True


def bad_command(response):
    '''handle bad commands'''
    global status
    status['resmessage'] = '[err]: bad command. type help to show usage'


def nop():
    '''nothing todo but change message'''
    global status
    status['resmessage'] = ''


def execute(command):
    global status

    # TODO: parser must recognize all operations
    if command == 'disconnect':
        disconnect()
        return

    # if connected before, you are in messagine mode until disconnect
    # so anything user types, sends to the other user
    if not status['command_mode']:
        send_message(command)
        return

    # in command mode we have commands and commands contains tokens
    # we should parse them and recognize the command
    tokens = tokenize(command)
    response = parse(tokens)

    # operation is exactly what parser returns
    operation = response['operation']

    # match command 
    if operation == LOGIN:
        login(response)
    elif operation == CONNECT:
        connect(response)
    elif operation == CREATE_GPGKEY:
        gpg_create_key(response)
    elif operation == SET_GPGPATH:
        gpg_set_directory(response)
    elif operation == ADD_CONTACT:
        add_contact(response)
    elif operation == EXPORT_GPG:
        gpg_export(response)
    elif operation == HELP:
        help()
    elif operation == EXIT:
        exit(0)
    elif operation == NOP:
        nop()
    else:
        bad_command(response)


