# simple parser for commands

# type of commands
BAD_COMMAND   =        0x00
CONNECT       =        0x01
DISCONNECT    =        0x02
LOGIN         =        0x03
HELP          =        0x04
CREATE_GPGKEY =        0x05
SET_GPGPATH   =        0x06
ADD_CONTACT   =        0x07
EXPORT_GPG    =        0x08
EXIT          =        0x09
NOP           =        0x90


def tokenize(command):
    '''
    tokenize the command
    @parameters: string
    @return: list<string>
    '''
    return command.split()


def parse(tokens):
    '''
    parse tokens to find out command type
    @parameters: list<string>
    @return: dict
    '''

    response = {'operation': BAD_COMMAND}

    if len(tokens) == 0:
        response['operation'] = NOP

    elif len(tokens) == 1:
        if tokens[0] == 'disconnect':
            response['operation'] = DISCONNECT
        elif tokens[0] == 'exit':
            response['operation'] = EXIT
        elif tokens[0] == 'help':
            response['operation'] = HELP

    elif len(tokens) == 2:
        if tokens[0] == 'connect':
            response['operation'] = CONNECT
            response['addr'] = tokens[1]
        elif tokens[0] == 'gpg-path':
            response['operation'] = SET_GPGPATH
            response['directory'] = tokens[1]

    elif len(tokens) == 3:
        if tokens[0] == 'login':
            response['operation'] = LOGIN
            response['username'] = tokens[1]
            response['password'] = tokens[2]
        elif tokens[0] == 'add':
            response['operation'] = ADD_CONTACT
            response['id'] = tokens[1]
            response['gpg_key'] = tokens[2]
        elif tokens[0] == 'gpg-create':
            response['operation'] = CREATE_GPGKEY 
            response['gpg_name'] = tokens[1]
            response['gpg_passphrase'] = tokens[2]
        elif tokens[0] == 'gpg-export':
            response['operation'] = EXPORT_GPG
            response['gpg_name'] = tokens[1]
            response['directory'] = tokens[2]

    return response


