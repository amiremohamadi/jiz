# using lms.birjand.ac.ir as a massenger server
# to be also available in National Information Network
# why using this shit?
#   SAFE: your messages are encrypted using GPG (PGP)
#   AVAILABLE: even in National Information Network
#   IT'S FUN :D
#
# change logs:
#   - login
#   - connect to user
#   - start conversation

from core import (execute, load_config, status)


VERSION = '0.0.2'

if __name__ == '__main__':
    welcome_ascii = '''
    / \  .-"""""-.  / \\
   (   \/ __   __ \/   )
    )  ; / _\ /_ \ ;  (
   (   |  / \ / \  |   )
    \ (,  \0/_\0/  ,) /
     \_|   /   \   |_/
       | (_\___/_) |
       .\ \ -.- / /.
      {  \ `===' /  }
     {    `.___.'    }
      {             }
       `"="="="="="`
    '''
    print(f'jizanthapus encrypted (version: {VERSION})')
    print(welcome_ascii)

    load_config()

    # REPL loop
    while True:
        command = input(f'[{status["userid"]}]: ')
        execute(command)
        print(status['resmessage'])
